import argparse
import datetime
import dateutil
import dateutil.parser
import os.path
import shutil
import subprocess
import time


def parse_args(desc):
    parser = argparse.ArgumentParser(description='Fetch GFS files.')
    parser.add_argument('--workdir', help='working directory', required=True)
    parser.add_argument('--grib-cdm-config', help='path to cdmGribReaderConfig.xml', required=True)
    parser.add_argument('--minlon', type=float, help='minimum longitude', required=True)
    parser.add_argument('--maxlon', type=float, help='maximum longitude', required=True)
    parser.add_argument('--minlat', type=float, help='minimum latitude',  required=True)
    parser.add_argument('--maxlat', type=float, help='maximum latitude',  required=True)
    parser.add_argument('--retrievals', help='ini file with retrievals', required=True)
    parser.add_argument('reftime', help="Model run")
    args = parser.parse_args()

    reftime = dateutil.parser.parse(args.reftime)

    year_month_day = f"{reftime.year:04d}{reftime.month:02d}{reftime.day:02d}"
    hour = f"{reftime.hour:02d}"
    bounds = [args.minlon, args.maxlon, args.minlat, args.maxlat]
    retrievals = load_retrievals(args.retrievals)

    return (args.workdir, args.grib_cdm_config, bounds, year_month_day, hour, retrievals)


def load_retrievals(retrievals_file):
    if retrievals_file.endswith('.json'):
        import json
        with open(retrievals_file, 'r') as rjs:
            return json.load(rjs)
    elif retrievals_file.endswith('.ini'):
        import configparser
        retrievals = []
        ini = configparser.SafeConfigParser()
        ini.read(retrievals_file)
        for name in ini.sections():
            levels = ini[name]['levels'].split()
            variables = ini[name]['variables'].split()
            retrievals.append([name, levels, variables])
        return retrievals
    else:
        raise Exception("retrievals file must have json or ini suffix")


def wait_for_file(session, gribFileUrl):
    while True:
        print(f"Looking for file '{gribFileUrl}' ...")
        h = session.head(gribFileUrl)
        if h.status_code == 200:
            lm = dateutil.parser.parse(h.headers['last-modified'])
            young = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(minutes=2)
            if lm < young:
                return
        print("File missing or too recent, waiting ...")
        time.sleep(60)


def file_exists(outputFile):
    if os.path.exists(outputFile):
        print(f"File '{outputFile}' exists, not downloading again.")
        return True
    else:
        return False


def fetch_grib(session, outputFile, url, levels, variables):
    if file_exists(outputFile):
        return False
    if len(levels) == 1 and levels[0] == "all":
        url += "&all_lev=on"
    else:
        for l in levels:
            url += f"&lev_{l}=on"
    if len(variables) == 1 and variables[0] == "all":
        url += "&all_var=on"
    else:
        for v in variables:
            url += f"&var_{v}=on"
    print(f"Fetching file '{outputFile}' from '{url}' ...")
    r = session.get(url)
    if r.status_code != 200:
        raise Exception(f"Error downloading '{url}', HTTP status code {r.status_code}")
    print(f"Finished downloading, HTTP status code {r.status_code}")
    with open(outputFile, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=1024):
            fd.write(chunk)
    return True


def update_grbml(gribIndex, grib_cdm_config, newFiles):
    if len(newFiles) == 0:
        return
    tmpGribIndex = gribIndex+".tmp"
    if os.path.exists(gribIndex):
        shutil.copy(gribIndex, tmpGribIndex)
    for nf in newFiles:
        subprocess.run(['fiIndexGribs', '--readerConfig', grib_cdm_config, '-a', tmpGribIndex, '-i', nf], check=True)
    shutil.move(tmpGribIndex, gribIndex)
