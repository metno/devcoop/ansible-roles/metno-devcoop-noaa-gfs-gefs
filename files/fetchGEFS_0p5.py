#!/usr/bin/env python3

import os
import requests
import sys

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

import noaa

workdir, grib_cdm_config, bounds, year_month_day, hour, retrievals = noaa.parse_args("Fetch GEFS files.")
llon, rlon, blat, tlat = bounds

s = requests.Session()

FilePostfix = ".t{}z.pgrb2a.0p50.f{:03d}"  # expandable with hour, timestep
PathUrl = f"https://nomads.ncep.noaa.gov/pub/data/nccf/com/gens/prod/gefs.{year_month_day}/{hour}/atmos/pgrb2ap5/"
FilterUrl = ("https://nomads.ncep.noaa.gov/cgi-bin/filter_gefs_atmos_0p50a.pl"
             + f"?dir=/gefs.{year_month_day}/{hour}/atmos/pgrb2ap5"
             + f"&subregion=&leftlon={llon}&rightlon={rlon}&toplat={tlat}&bottomlat={blat}")

members = ["gec00"] + ["gep{:02d}".format(i+1) for i in range(30)]
timeSteps = [6*t for t in range(29)]

gribDir = os.path.join(workdir, f"gefs_0p5_{year_month_day}_{hour}")
os.makedirs(gribDir, exist_ok=True)

gribIndex = gribDir + ".grbml"

for timestep in timeSteps:
    for member in members:
        gribFile = member + FilePostfix.format(hour, timestep)
        gribFileUrl = PathUrl + gribFile

        waited_for_file = False

        # download data
        newFiles = []
        for name, levels, variables in retrievals:
            outputFile = os.path.join(gribDir, f"gefs_{member}_{name}.f{timestep:03d}")
            if noaa.file_exists(outputFile):
                continue

            if not waited_for_file:
                waited_for_file = True
                noaa.wait_for_file(s, gribFileUrl)

            url = FilterUrl + '&file=' + gribFile
            if noaa.fetch_grib(s, outputFile, url, levels, variables):
                newFiles.append(outputFile)

        noaa.update_grbml(gribIndex, grib_cdm_config, newFiles)
