#! /usr/bin/env python3

import os
import requests
import sys

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

import noaa

workdir, grib_cdm_config, bounds, year_month_day, hour, retrievals = noaa.parse_args("Fetch GFS files.")
llon, rlon, blat, tlat = bounds

s = requests.Session()

FileFormat = "gfs.t{}z.pgrb2.0p25.f{:03d}"  # expandable with hour, timestep
PathUrl = f"https://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/gfs.{year_month_day}/{hour}/atmos/"
FilterUrl = ("https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl"
             + f"?subregion=&leftlon={llon}&rightlon={rlon}&toplat={tlat}&bottomlat={blat}"
             + f"&dir=/gfs.{year_month_day}/{hour}/atmos")

timeSteps = [3*t for t in range(21)]      # 3 hourly, 0-60
timeSteps += [66+6*t for t in range(10)]  # 6 hourly, 60-120

gribDir = os.path.join(workdir, f"gfs_0p25_{year_month_day}_{hour}")
os.makedirs(gribDir, exist_ok=True)

gribIndex = gribDir + ".grbml"

for timestep in timeSteps:
    gribFile = (FileFormat).format(hour, timestep)
    gribFileUrl = PathUrl + gribFile

    waited_for_file = False

    # download data
    newFiles = []
    for name, levels, variables in retrievals:
        outputFile = os.path.join(gribDir, f"gfs_{name}.f{timestep:03d}")
        if noaa.file_exists(outputFile):
            continue

        if not waited_for_file:
            # check for available data
            waited_for_file = True
            noaa.wait_for_file(s, gribFileUrl)

        url = FilterUrl + '&file=' + gribFile
        if noaa.fetch_grib(s, outputFile, url, levels, variables):
            newFiles.append(outputFile)

    noaa.update_grbml(gribIndex, grib_cdm_config, newFiles)
